Code of Conduct
===============

:slug: code-of-conduct
:category: About
:date: 2020-01-02

The Linux kernel community operates a `Code of Conduct`_ based on the
`Contributor Covenant Code of Conduct`_ with a `Linux Kernel Contributor Covenant Code of Conduct Interpretation`_.

.. _`Code of Conduct`: https://www.kernel.org/doc/html/latest/process/code-of-conduct.html
.. _`Contributor Covenant Code of Conduct`: https://www.contributor-covenant.org/version/1/4/code-of-conduct.html
.. _`Linux Kernel Contributor Covenant Code of Conduct Interpretation`: https://www.kernel.org/doc/html/latest/process/code-of-conduct-interpretation.html

Code of Conduct Committee
-------------------------

The Linux kernel Code of Conduct Committee is currently made up of the
following people:

  - Kristen Accardi <kristen.c.accardi@intel.com>
  - Shuah Khan <skhan@linuxfoundation.org>
  - Greg Kroah-Hartman <gregkh@linuxfoundation.org>
  - Joanna Lee <jlee@linuxfoundation.org>

Committee members can be reached all at once by writing to
<conduct@kernel.org>.

Committee Reports
-----------------

We would like to thank the Linux kernel community members who have supported
the adoption of the Code of Conduct and who continue to uphold the professional
standards of our community. If you have any questions about these reports,
please write to <conduct@kernel.org>.

March 2024
~~~~~~~~~~
Archival copy: https://lore.kernel.org/r/355aee5f-13ce-4e20-9ce8-e5bcddd14bc2@linuxfoundation.org

In the period of October 1, 2023 through March 31, 2024, the Code of
Conduct Committee received the following reports:

- Unprofessional behavior or comments in email: 2

The result of the investigation:

- Education and coaching clarifying the role of Code of Conduct
  conduct on conversations that don't go against the CoC.
- Education and coaching the individuals on the impact of making
  unprofessional comments which could be misunderstood leading
  to negative impressions about the community.

The reports were about the offhand comments made while rejecting
the code which are not violations of the Code of Conduct

Unacceptable behavior or comments on a private invitee only chat
channel: 1

- Education and coaching clarifying the role of Code of Conduct
  conduct on conversations that occur on a private chat channel.

We would like to thank the Linux kernel community members who have
supported the adoption of the Code of Conduct and who continue to
uphold the professional standards of our community. If you have
questions about this report, please write to <conduct@kernel.org>.

September 2023
~~~~~~~~~~~~~~
Archival copy: https://lore.kernel.org/r/3351be6b-854e-479d-832c-83cb8829c010@linuxfoundation.org

In the period of April 1, 2023 through September 30, 2023, the Code of
Conduct Committee received the following reports:

- Unacceptable behavior or comments in email: 4

The result of the investigation:

- Education and coaching clarifying the Code of Conduct conduct related
  to normal review and patch acceptance process: 3
- Clarification on the Code of Conduct conduct related to maintainer
  rights and responsibility to reject code: 1

The reports were about the discussion during the patch review and
decisions made in rejecting code and these actions are not viewed as
violations of the Code of Conduct.

Please see the excerpt from the Responsibilities section in the `Linux
Kernel Contributor Covenant Code of Conduct Interpretation`_ document::

    setting expertise expectations, making decisions and rejecting unsuitable
    contributions are not viewed as a violation of the Code of Conduct.

March 2023
~~~~~~~~~~
Archival copy: https://lore.kernel.org/r/557ef895-ad2d-eff9-7cb8-70dbcf41adea@linuxfoundation.org

In the period of October 1, 2022 through March 31, 2023, the Code of
Conduct Committee received the following reports:

  - Unacceptable behavior or comments in email: 6

The result of the investigation:

 - Education and coaching clarifying the Code of Conduct conduct related
   to normal review and patch acceptance process: 1
 - Clarification on the Code of Conduct conduct related to maintainer
   rights and responsibility to reject code: 5

The reports were about the decisions made in rejecting code and these
actions are not viewed as violations of the Code of Conduct.

Please see the excerpt from the Responsibilities section in the `Linux
Kernel Contributor Covenant Code of Conduct Interpretation`_ document::

    setting expertise expectations, making decisions and rejecting unsuitable
    contributions are not viewed as a violation of the Code of Conduct.


September 2022
~~~~~~~~~~~~~~
Archival copy: https://lore.kernel.org/r/57a492fb-928b-9e0a-5f0e-dc95ef599309@linuxfoundation.org

In the period of April 1, 2022 through September 30, 2022, the Code of
Conduct Committee received the following reports:

  - Unacceptable behavior or comments in email: 1

The result of the investigation:

  - Resolved with a public apology from the violator with a commitment
    from them to abide by the Code of Conduct in the future.

March 2022
----------

Archival copy: https://lore.kernel.org/r/4401af50-083d-0239-6b7f-3454c8d69fec@linuxfoundation.org

In the period of October 1, 2021 through March 31, 2022, the Code of
Conduct Committee received the following reports:

  - Unacceptable behavior or comments in email: 2

The result of the investigation:

  - Education and coaching clarifying the Code of Conduct conduct
    related to normal review process: 2

September 2021
~~~~~~~~~~~~~~

Archival copy: https://lore.kernel.org/r/e81f0726-5f8f-f10f-d926-a9126941d38e@linuxfoundation.org

In the period of May 1, 2021 through September 30, 2021, the Code of
Conduct Committee received the following reports:

  - Unacceptable behavior or comments in email: 1

The result of the investigation:

  - Education and coaching clarifying the Code of Conduct conduct
    related to normal review process: 1

April 2021
~~~~~~~~~~

Archival copy: https://lore.kernel.org/r/448b06e4-41fc-26df-a862-c3ba2f70b6b3@linuxfoundation.org

In the period of November 1, 2020 through April 30, 2021 the Code of
Conduct Committee received the following reports:

  - Unacceptable behavior or comments in email (3rd party): 4

The result of the investigation:

  - Education and coaching: 1
  - Public response to call attention to the behavior and request
    correction with consequence of ban if behavior persists: 1
  - Public response to attention to the behavior and request correction: 1
  - Clarification on the Code of Conduct conduct related to maintainer
    rights and responsibility to reject code: 1

October 2020
~~~~~~~~~~~~

Archival copy: https://lore.kernel.org/lkml/20201105083002.GA3429143@kroah.com/

In the period of January 1, 2020 through October 31, 2020 the Committee
received the following reports:

  - Unacceptable behavior or comments in email: 1
  - Unacceptable comments in github repo by non-community members: 1
  - Unacceptable comments toward a company: 1

The result of the investigation:

  - Education and coaching: 1
  - Locking of github repo for any comments: 1
  - Clarification that the Code of Conduct covers conduct related to individual developers only: 1

December 2019
~~~~~~~~~~~~~

Archival copy: https://lore.kernel.org/lkml/20200103105614.GC1047442@kroah.com/

In the period of December 1, 2019 through December 30, 2019 the Committee
received the following report:

  - Insulting behavior in email: 1

The result of the investigation:

  - Education and coaching: 1

August to November 2019
~~~~~~~~~~~~~~~~~~~~~~~

Archival copy: https://lore.kernel.org/lkml/20191218090054.GA5120@kroah.com/

In the period of August 1, 2019 through November 31, 2019, the Committee
received no reports.

September 2018 to July 2019
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Archival copy: https://lore.kernel.org/lkml/20190810120700.GA7360@kroah.com/

In the period of September 15, 2018 through July 31, 2019, the Committee
received the following reports:

  - Inappropriate language in the kernel source: 1
  - Insulting behavior in email: 3

The result of the investigations:

  - Education and coaching: 4
