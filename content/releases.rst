Active kernel releases
======================

:slug: releases
:category: Releases

There are several main categories into which kernel releases may fall:

Prepatch
    Prepatch or "RC" kernels are mainline kernel pre-releases that are
    mostly aimed at other kernel developers and Linux enthusiasts. They
    must be compiled from source and usually contain new features that
    must be tested before they can be put into a stable release.
    Prepatch kernels are maintained and released by Linus Torvalds.

Mainline
    Mainline tree is maintained by Linus Torvalds. It's the tree where
    all new features are introduced and where all the exciting new
    development happens. New mainline kernels are released every 9-10
    weeks.

Stable
    After each mainline kernel is released, it is considered "stable."
    Any bug fixes for a stable kernel are backported from the mainline
    tree and applied by a designated stable kernel maintainer. There are
    usually only a few bugfix kernel releases until next mainline kernel
    becomes available -- unless it is designated a "longterm maintenance
    kernel." Stable kernel updates are released on as-needed basis,
    usually once a week.

Longterm
    There are usually several "longterm maintenance" kernel releases
    provided for the purposes of backporting bugfixes for older kernel
    trees. Only important bugfixes are applied to such kernels and they
    don't usually see very frequent releases, especially for older
    trees.

.. table:: Longterm release kernels

    ======== ================================ ============ ==================
    Version  Maintainer                       Released     Projected EOL
    ======== ================================ ============ ==================
    6.12     Greg Kroah-Hartman & Sasha Levin 2024-11-17   Dec, 2026
    6.6      Greg Kroah-Hartman & Sasha Levin 2023-10-29   Dec, 2026
    6.1      Greg Kroah-Hartman & Sasha Levin 2022-12-11   Dec, 2027
    5.15     Greg Kroah-Hartman & Sasha Levin 2021-10-31   Dec, 2026
    5.10     Greg Kroah-Hartman & Sasha Levin 2020-12-13   Dec, 2026
    5.4      Greg Kroah-Hartman & Sasha Levin 2019-11-24   Dec, 2025
    ======== ================================ ============ ==================

Distribution kernels
--------------------
Many Linux distributions provide their own "longterm maintenance"
kernels that may or may not be based on those maintained by kernel
developers. These kernel releases are not hosted at kernel.org and
kernel developers can provide no support for them.

It is easy to tell if you are running a distribution kernel. Unless you
downloaded, compiled and installed your own version of kernel from
kernel.org, you are running a distribution kernel. To find out the
version of your kernel, run `uname -r`::

    # uname -r
    5.6.19-300.fc32.x86_64

If you see anything at all after the dash, you are running a distribution
kernel. Please use the support channels offered by your distribution
vendor to obtain kernel support.

Releases FAQ
------------
Here are some questions we routinely receive about kernel release
versions. See also the main "FAQ" section for some other topics.

When is the next mainline kernel version going to be released?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Linux kernel follows a simple release cadence:

- after each mainline release, there is a 2-week "merge window" period
  during which new major features are introduced into the kernel
- after the merge window closes, there is a 7-week bugfix and
  stabilization period with weekly "release candidate" snapshots
- rc7 is usually the last release candidate, though occasionally there
  may be additional rc8+ releases if that is deemed necessary

So, to find the approximate date of the next mainline kernel release,
take the date of the previous mainline release and add 9-10 weeks.

What is the next longterm release going to be?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Longterm kernels are picked based on various factors -- major new
features, popular commercial distribution needs, device manufacturer
demand, maintainer workload and availability, etc. You can roughly
estimate when the new longterm version will become available based on
how much time has elapsed since the last longterm version was chosen.

Why are some longterm versions supported longer than others?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The "projected EOL" dates are not set in stone. Each new longterm kernel
usually starts with only a 2-year projected EOL that can be extended
further if there is enough interest from the industry at large to help
support it for a longer period of time.

Does the major version number (4.x vs 5.x) mean anything?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
No. The major version number is incremented when the number after the
dot starts looking "too big." There is literally no other reason.

Does the odd-even number still mean anything?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A long time ago Linux used a system where odd numbers after the first
dot indicated pre-release, development kernels (e.g. 2.1, 2.3, 2.5).
This scheme was abandoned after the release of kernel 2.6 and these days
pre-release kernels are indicated with "-rc".
